using System;
using System.Collections.Generic;

namespace INF1035_COURAULT_DEDROUAS_PECHE
{
    class Player
    {
        private string firstName;
        private string name;
        private List<Card> hand = new List<Card>();

        public Player(string nickname, string name)
        {
            this.firstName = nickname;
            this.name = name;
        }

        public string FirstName => firstName;

        public string Name => name;

        public void getCard(Card c)
        {
            hand.Add(c);
        }

        public List<Card> Hand
        {
            get => hand;
        }
    }
}