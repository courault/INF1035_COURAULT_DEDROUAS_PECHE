using System;
using System.IO;

namespace INF1035_COURAULT_DEDROUAS_PECHE
{
    class Display
    {
        private int NUM_PLAYERS;    //Nombre de joueurs

        public Display()
        {
            PrintF("res/card");
            NUM_PLAYERS = Int32.Parse(PrintNRead("Veuillez entrer le nombre de joueurs (2 à 4) :"));
            Player[] players = new Player[NUM_PLAYERS];
            string firstName, name;
            for(var i = 0; i < NUM_PLAYERS; i++)
            {
                Console.Clear();
                firstName = PrintNRead("Entrez le prénom du joueur " + (i+1) + " :");
                name = PrintNRead("Entrez le nom du joueur " + (i+1) + " :");
                players[i] = new Player(firstName, name);
            }
            new Game(players);
        }

        //Affiche la chaîne txt
        public void PrintS(string txt)
        {
            Console.WriteLine(txt);
        }

        //Affiche le contenu du fichier file
        public void PrintF(String file)
        {
            try 
            { 
                StreamReader stR = new StreamReader(file);  //Création d'une instance de StreamReader pour lire le fichier 
                string txt = stR.ReadToEnd();   //Lit l'intégralité du fichier
                PrintS(txt);
                stR.Close();    //Fermeture du StreamReader
            }
            catch (Exception) 
            {
                PrintS("Unable to read the file : " + file);
            }
        }

        public String Read()
        {
            return Console.ReadLine();
        }

        public String PrintNRead(String txt)
        {
            PrintS(txt);
            return Read();
        }

        static void Main(string[] args)
        {
            new Display();
        }
    }
}