﻿using System;
using System.Collections.Generic;

namespace INF1035_COURAULT_DEDROUAS_PECHE
{
    class SetOfCards
    {
        //Pioche
        private List<Card> draw = new List<Card>();
        
        //Défausse (pile de dépôt)
        private List<Card> deposit = new List<Card>();

        //Initialisation du jeu de cartes : Jeu de 52 cartes
        public SetOfCards()
        {
            foreach (var c in Enum.GetValues(typeof(Card.Color)))
                foreach (var v in Enum.GetValues(typeof(Card.Value)))
                    draw.Add(new Card((Card.Value)v, (Card.Color)c));
        }

        //Mélange le jeu de cartes aléatoirement à l'aide de l'algorithme de Fisher-Yates
        public void MixDraw()
        {
            Random rnd = new Random();
            Card tmpCard;
            for(int i = draw.Count-1, j; i > 1; i--)
            {
                j = rnd.Next(i+1);  //Nombre aléatoire entre 0 et i inclus
                tmpCard = draw[j];  //Permutation
                draw[j] = draw[i];  //De l'élément i
                draw[i] = tmpCard;  //Avec l'élément j
            }
        }

        //Retire une carte de la pioche
        public Card DrawCard()
        {
            Card drawnCard = draw[0];
            draw.Remove(drawnCard);
            return drawnCard;
        }

        //Pose la carte c sur la défausse
        public void DiscardCard(Card c)
        {
            deposit.Add(c);
        }
    }
}
