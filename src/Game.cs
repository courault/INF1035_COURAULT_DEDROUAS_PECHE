using System;
using System.Collections.Generic;

namespace INF1035_COURAULT_DEDROUAS_PECHE
{
    class Game
    {
        public const int NUM_DRAWN_CARDS = 8;  //Nombre de cartes piochés par joueur au début de la partie

        private Player[] players;
        private SetOfCards set;

        public Game(Player[] players)
        {
            this.players = players;
            set = new SetOfCards();
            Start();
        }

        public void Start()
        {
            set.MixDraw();
            DistributeCards();
        }

        //Technique de distribution puriste
        public void DistributeCards()
        {
            for(var i = 0; i < NUM_DRAWN_CARDS; i++)
                for(var j = 0; j < players.Length; j++)
                    players[j].getCard(set.DrawCard());

            /*for(var j = 0; j < players.Length; j++){
                Console.WriteLine("Player "+j);
                for(var k = 0; k < players[0].Hand.Count; k++)
                    Console.WriteLine(" "+players[j].Hand[k]);
            }*/
        }
    }
}