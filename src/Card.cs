using System;

namespace INF1035_COURAULT_DEDROUAS_PECHE
{
    class Card
    {
        public enum Value { Ace, Two, Tree, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King }
        public enum Color { Clover, Tile, Heart, Pike }

        private Value value;
        private Color color;

        public Card(Value value, Color color)
        {
            this.value = value;
            this.color = color;
        }

        public override string ToString()
        {
            return value + " of " + color;
        }
    }
}